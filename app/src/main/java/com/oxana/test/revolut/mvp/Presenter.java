package com.oxana.test.revolut.mvp;

import com.oxana.test.revolut.mvp.model.AbstractRepository;
import com.oxana.test.revolut.mvp.model.IEntity;
import com.oxana.test.revolut.mvp.model.RatesModel;
import com.oxana.test.revolut.mvp.view.IView;

import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Oksana_Skorniakova on 2/28/17.
 */

public class Presenter extends BasePresenter<IView, AbstractRepository> implements IView.OnSumEnteredListener, IView.OnCurrencySelected, Observer {
    private final int UPDATE_DELAY = 30 * 1000;

    private Timer timer = new Timer();
    private TimerTask task;

    public Presenter(IView view, AbstractRepository abstractRepository) {
        super(view, abstractRepository);
    }


    public void attach() {
        getRepo().addObserver(this);
        getView().setSumEnteredListener(this);
        getView().setCurrencySelectedListener(this);
        restart();
    }

    public void setValues() {
        RatesModel model = getRepo().getRates(getView().getBaseCurrency(), false);
        if (model != null) {
            getView().setRates(model.getRate(getView().getConvertCurrency()));
            getView().setSum(countSum(getView().getEnteredSum()));
        }
    }

    public void detach() {
        getRepo().deleteObserver(this);
        getView().shutDown();
        if (task != null) {
            task.cancel();
        }
    }


    @Override
    public void onCurrencySelected() {
        restart();
    }

    @Override
    public void onSumEntered() {
        getView().setSum(countSum(getView().getEnteredSum()));
    }

    private Double countSum(Double sum) {
        if (sum == null || sum == 0) {
            sum = 0d;
        } else {
            sum = sum * getRepo().getRates(getView().getBaseCurrency(), false).getRate(getView().getConvertCurrency());
        }
        return sum;
    }

    private void restart() {
        getRepo().restart();
        if (task != null) {
            task.cancel();
        }

        task = new TimerTask() {
            @Override
            public void run() {
                getRepo().getRates(getView().getBaseCurrency(), true);
            }
        };
        timer.scheduleAtFixedRate(task, 0, UPDATE_DELAY);
    }

    @Override
    public void update(Observable observable, Object o) {

        switch (getRepo().getState()) {
            case IEntity.EntityState.AVAILABLE: {
                if (o instanceof String) {
                    if (o.equals(getView().getBaseCurrency())) {
                        setValues();
                    }
                }

                break;
            }

            case IEntity.EntityState.NA:
            case IEntity.EntityState.LOADING:
            default: {
                // todo: display somehow
                break;
            }
        }

    }
}
