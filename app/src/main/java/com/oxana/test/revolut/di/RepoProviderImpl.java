package com.oxana.test.revolut.di;

import com.oxana.test.revolut.network.Service;
import com.oxana.test.revolut.mvp.model.Repository;

import javax.inject.Inject;

/**
 * Created by Oksana_Skorniakova on 3/1/17.
 */

public class RepoProviderImpl implements IRepoProvider {

    private final Repository rates;


    @Inject
    RepoProviderImpl(Service restService) {
        rates = new Repository(restService.getApi());
    }

    @Override
    public Repository getRepository() {
        return rates;
    }
}
