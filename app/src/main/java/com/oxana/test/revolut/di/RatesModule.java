package com.oxana.test.revolut.di;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Oksana_Skorniakova on 3/1/17.
 */

@Module
public class RatesModule {
    @Provides
    IRepoProvider provideRates(RepoProviderImpl impl) {
        return impl;
    }
}
