package com.oxana.test.revolut.mvp.model;

/**
 * Created by Oksana_Skorniakova on 2/28/17.
 */

public class RateModel {

    private final
    @RatesModel.Currency
    String currency;
    private Double rate;

    public RateModel(String currency, Double rate) {
        this.currency = currency;
        this.rate = rate;
    }

    public RateModel(String currency) {
        this(currency, null);
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RateModel) {
            RateModel model = (RateModel) obj;
            if (this.currency.equals(model.getCurrency()) && this.rate == model.getRate()) {
                return true;
            }
        }
        return false;
    }


    @Override
    public int hashCode() {
        return currency.hashCode();
    }
}
