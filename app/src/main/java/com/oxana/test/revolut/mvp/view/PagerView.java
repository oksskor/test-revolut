package com.oxana.test.revolut.mvp.view;

import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.TextView;

import com.oxana.test.revolut.R;
import com.oxana.test.revolut.mvp.model.RatesModel;
import com.oxana.test.revolut.utils.Indicator;
import com.oxana.test.revolut.utils.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Oksana_Skorniakova on 3/2/17.
 */

public class PagerView implements IView, ViewPager.OnPageChangeListener, TextWatcher {
    private final ViewPager pagerBase;
    private final ViewPager pagerConvert;

    private OnSumEnteredListener sumListener;
    private OnCurrencySelected currencySelectedListener;

    //for showing updating
    private Animation anim;

    private List<ViewHolder> pagesBase = new ArrayList<>();
    private List<ViewHolder> pagesConvert = new ArrayList<>();

    String[] currency = new String[]{RatesModel.Currency.EUR, RatesModel.Currency.USD, RatesModel.Currency.GBP};


    public PagerView(ViewPager pagerBase, ViewPager pagerConvert, Indicator indicatorBase, Indicator indicatorConvert) {

        this.pagerBase = pagerBase;
        this.pagerConvert = pagerConvert;

        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(50);
        anim.setRepeatCount(0);


        LayoutInflater inflater = LayoutInflater.from(pagerBase.getContext());

//set Base pager
        for (int i = 0; i < currency.length; i++) {
            ViewHolder vh = new ViewHolder((ViewGroup) inflater.inflate(R.layout.pager_item, null));
            vh.sum.addTextChangedListener(this);
            vh.currency.setText(currency[i]);
            pagesBase.add(vh);
        }
        MyPagerAdapter adapterBase = new MyPagerAdapter(pagesBase);
        pagerBase.setAdapter(adapterBase);
        pagerBase.addOnPageChangeListener(this);
        indicatorBase.setUpWithViewPager(pagerBase);
        pagerBase.setCurrentItem(0);

//set Convert pager
        for (int i = 0; i < currency.length; i++) {
            ViewHolder vh = new ViewHolder((ViewGroup) inflater.inflate(R.layout.pager_item, null));
            vh.currency.setText(currency[i]);
            vh.sum.setClickable(false);
            vh.sum.setFocusableInTouchMode(false);
            vh.sum.setFocusable(false);
            vh.sum.setBackgroundColor(Color.TRANSPARENT);
            pagesConvert.add(vh);
        }

        MyPagerAdapter adapterConvert = new MyPagerAdapter(pagesConvert);
        pagerConvert.setAdapter(adapterConvert);
        pagerConvert.addOnPageChangeListener(this);
        indicatorConvert.setUpWithViewPager(pagerConvert);
        pagerConvert.setCurrentItem(1);

    }

    @Override
    public String getBaseCurrency() {
        return currency[pagerBase.getCurrentItem()];
    }

    @Override
    public String getConvertCurrency() {
        return currency[pagerConvert.getCurrentItem()];
    }

    @Override
    public Double getEnteredSum() {
        Double d = 0d;
        if (!getBaseCurrency().equals(getConvertCurrency())) {
            String str = String.valueOf(pagesBase.get(pagerBase.getCurrentItem()).sum.getText());
            if (str != null && str.length() > 0) {
                d = Double.parseDouble(str);
            }
        }
        return d;
    }

    @Override
    public void shutDown() {
        sumListener = null;
        currencySelectedListener = null;
    }

    @Override
    public void setRates(Double rate) {
        if (rate != null) {

            pagesBase.get(pagerBase.getCurrentItem()).rate.setText(String.format("%.4f", rate));
            pagesConvert.get(pagerConvert.getCurrentItem()).rate.setText(String.format("%.4f", 1 / rate));

            pagesBase.get(pagerBase.getCurrentItem()).rate.startAnimation(anim);
            pagesConvert.get(pagerConvert.getCurrentItem()).rate.startAnimation(anim);
        }
    }

    @Override
    public void setSum(Double sum) {
        pagesConvert.get(pagerConvert.getCurrentItem()).sum.setText(String.format("%.2f", sum));
        pagesConvert.get(pagerConvert.getCurrentItem()).sum.startAnimation(anim);
    }

    @Override
    public void setSumEnteredListener(OnSumEnteredListener listener) {
        this.sumListener = listener;
    }

    @Override
    public void setCurrencySelectedListener(OnCurrencySelected listener) {
        this.currencySelectedListener = listener;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        if (getBaseCurrency().equals(getConvertCurrency())) {
            pagesBase.get(pagerBase.getCurrentItem()).hideRate(true);
            pagesConvert.get(pagerConvert.getCurrentItem()).setSame(true);
        } else {
            pagesConvert.get(pagerConvert.getCurrentItem()).setSame(false);
            pagesBase.get(pagerBase.getCurrentItem()).hideRate(false);
            pagesBase.get(pagerBase.getCurrentItem()).setLabel(getBaseCurrency(), getConvertCurrency());
            pagesConvert.get(pagerConvert.getCurrentItem()).setLabel(getConvertCurrency(), getBaseCurrency());

            if (currencySelectedListener != null) {
                currencySelectedListener.onCurrencySelected();
            }
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (sumListener != null && getEnteredSum() != null) {
            sumListener.onSumEntered();
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    public class ViewHolder {
        View root;
        @BindView(R.id.sum)
        EditText sum;
        @BindView(R.id.label)
        TextView label;
        @BindView(R.id.rate)
        TextView rate;
        @BindView(R.id.cur)
        TextView currency;
        @BindView(R.id.same)
        TextView same;
        @BindView(R.id.layout)
        ViewGroup layout;

        public ViewHolder(ViewGroup view) {
            this.root = view;
            ButterKnife.bind(this, view);
        }

        public void setLabel(@RatesModel.Currency String base, @RatesModel.Currency String convert) {
            String text = root.getResources().getString(R.string.label, Util.getSymbol(root.getContext(), base), Util.getSymbol(root.getContext(), convert));
            label.setText(text);

        }

        public void setSame(boolean isSame) {
            if (isSame) {
                layout.setVisibility(View.INVISIBLE);
                same.setVisibility(View.VISIBLE);
            } else {
                layout.setVisibility(View.VISIBLE);
                same.setVisibility(View.INVISIBLE);
            }
        }

        public void hideRate(boolean hide) {
            if (hide) {
                rate.setVisibility(View.INVISIBLE);
                label.setVisibility(View.INVISIBLE);
            } else {
                rate.setVisibility(View.VISIBLE);
                label.setVisibility(View.VISIBLE);
            }
        }

    }

}
