package com.oxana.test.revolut.utils;

import android.content.Context;

import com.oxana.test.revolut.R;
import com.oxana.test.revolut.mvp.model.RatesModel;

/**
 * Created by Oksana_Skorniakova on 3/3/17.
 */

public class Util {

    public static String getSymbol(Context contex, @RatesModel.Currency String currency) {
        switch (currency) {
            case RatesModel.Currency.EUR:
                return contex.getResources().getString(R.string.euro);
            case RatesModel.Currency.USD:
                return contex.getResources().getString(R.string.usd);
            case RatesModel.Currency.GBP:
                return contex.getResources().getString(R.string.gbp);
        }
        return "";
    }
}
