package com.oxana.test.revolut.mvp.model;

import com.oxana.test.revolut.network.API;
import com.oxana.test.revolut.network.RatesJson;

import java.util.HashSet;
import java.util.Set;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Oksana_Skorniakova on 2/28/17.
 */

public class Repository extends AbstractRepository {
    private final API api;
    private final Set<RatesModel> models = new HashSet<>();

    public Repository(API api) {
        this.api = api;
    }

    @Override
    public void restart() {
        models.clear();
    }

    @Override
    public void update(final @RatesModel.Currency String baseCurrency) {

        api
                .getRates(baseCurrency)
                .subscribeOn(Schedulers.newThread())
                .map(new Func1<RatesJson, RatesModel>() {
                    @Override
                    public RatesModel call(RatesJson rateJson) {
                        RatesModel model = new RatesModel(rateJson);
                        return model;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RatesModel>() {
                    @Override
                    public final void onCompleted() {
                    }

                    @Override
                    public final void onError(Throwable e) {
                        updated(IEntity.EntityState.LAST_WITH_ERROR, null);
                    }

                    @Override
                    public void onNext(RatesModel model) {
                        //it's better to add check if new rates are the same, and update only when they differs
                        models.add(model);
                        updated(IEntity.EntityState.AVAILABLE, baseCurrency);
                    }

                });

    }

    @Override
    public RatesModel getRates(@RatesModel.Currency String baseCurrency, boolean update) {
        if (update)
            update(baseCurrency);
        for (RatesModel ratesModel : models) {
            if (ratesModel.getBase().equals(baseCurrency))
                return ratesModel;
        }
        return null;
    }
}
