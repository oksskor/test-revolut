package com.oxana.test.revolut.mvp.view;

import com.oxana.test.revolut.mvp.model.RatesModel;

/**
 * Created by Oksana_Skorniakova on 2/28/17.
 */

public interface IView {


    @RatesModel.Currency String getBaseCurrency();
    @RatesModel.Currency String getConvertCurrency();
    Double getEnteredSum();

    void shutDown();
    void setRates(Double rate);
    void setSum(Double sum);


    void setSumEnteredListener(OnSumEnteredListener listener);
    void setCurrencySelectedListener(OnCurrencySelected listener);

    public interface OnSumEnteredListener{
        void onSumEntered();
    }


    public interface OnCurrencySelected{
        void onCurrencySelected();
    }
}
