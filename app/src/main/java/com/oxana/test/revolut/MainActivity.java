package com.oxana.test.revolut;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.oxana.test.revolut.di.IRepoProvider;

import javax.inject.Inject;

import static com.oxana.test.revolut.di.MainActivityComponent.Injector.inject;

public class MainActivity extends AppCompatActivity  implements IApiProvider {

    @Inject
    public IRepoProvider api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        inject(this);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, new PagerFragment());
        fragmentTransaction.commit();
    }

    @Override
    public IRepoProvider getApi() {
        return api;
    }

}
