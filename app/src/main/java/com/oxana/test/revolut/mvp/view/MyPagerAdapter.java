package com.oxana.test.revolut.mvp.view;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Oksana_Skorniakova on 3/3/17.
 */

public class MyPagerAdapter extends PagerAdapter {

    List<PagerView.ViewHolder> pages = null;


    public MyPagerAdapter(List<PagerView.ViewHolder> pages) {
        this.pages = pages;
    }

    @Override
    public int getCount() {
        return pages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        if (object instanceof PagerView.ViewHolder) {
            return view.equals(((PagerView.ViewHolder) object).root);
        }
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = pages.get(position).root;
        container.addView(v, 0);
        return v;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
