package com.oxana.test.revolut.di;

import com.oxana.test.revolut.network.Service;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Oksana_Skorniakova on 3/1/17.
 */

@Module
public class ServiceModule {
    @Provides
    Service provideService(Service impl) {
        return impl;
    }
}