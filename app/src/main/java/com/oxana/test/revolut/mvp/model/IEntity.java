package com.oxana.test.revolut.mvp.model;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import static com.oxana.test.revolut.mvp.model.IEntity.EntityState.*;

/**
 * Created by Oksana_Skorniakova on 2/28/17.
 */

public interface IEntity {

    @EntityState int getState();

    @IntDef({NA, LOADING, AVAILABLE, LAST_WITH_ERROR})
    @Retention(RetentionPolicy.SOURCE)
    @interface EntityState {
        int NA = 0;
        int LOADING = 1;
        int AVAILABLE = 2;
        int LAST_WITH_ERROR = 3;
    }
}
