package com.oxana.test.revolut.di;

import com.oxana.test.revolut.mvp.model.Repository;

/**
 * Created by Oksana_Skorniakova on 3/1/17.
 */

public interface IRepoProvider {
    Repository getRepository();

}
