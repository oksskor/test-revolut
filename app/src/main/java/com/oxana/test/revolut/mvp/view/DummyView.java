package com.oxana.test.revolut.mvp.view;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.TextView;

import com.oxana.test.revolut.mvp.model.RatesModel;

/**
 * Created by Oksana_Skorniakova on 2/28/17.
 */

public class DummyView implements IView {

    private
    @RatesModel.Currency
    String baseCurrency = RatesModel.Currency.EUR;
    private
    @RatesModel.Currency
    String convertCurrency = RatesModel.Currency.USD;

    private EditText sum1;
    private TextView sum2;
    private TextView rate1;
    private TextView rate2;

    private OnSumEnteredListener sumListener;
    private OnCurrencySelected currencySelectedListener;

    //for showing updating
    private Animation anim;

    public DummyView(final EditText sum1, TextView sum2, TextView rate1, TextView rate2) {
        this.sum1 = sum1;
        this.sum2 = sum2;
        this.rate1 = rate1;
        this.rate2 = rate2;


        anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(50);
        anim.setRepeatCount(0);

        sum1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (sumListener != null && sum1.getText() != null && sum1.getText().length() > 0) {
                    sumListener.onSumEntered();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    @Override
    public String getBaseCurrency() {
        return baseCurrency;
    }

    @Override
    public String getConvertCurrency() {
        return convertCurrency;
    }

    @Override
    public Double getEnteredSum() {
        return Double.parseDouble(String.valueOf(sum1.getText()));
    }

    @Override
    public void shutDown() {
        sumListener = null;
        currencySelectedListener = null;
    }

    @Override
    public void setRates(Double rate) {
        this.rate1.setText(String.format("%.4f", rate));
        this.rate2.setText(String.format("%.4f", 1 / rate));

        rate1.startAnimation(anim);
        rate2.startAnimation(anim);
    }

    @Override
    public void setSum(Double sum) {
        sum2.setText(String.format("%.2f", sum));
        sum2.startAnimation(anim);
    }

    @Override
    public void setSumEnteredListener(OnSumEnteredListener listener) {
        this.sumListener = listener;
    }

    @Override
    public void setCurrencySelectedListener(OnCurrencySelected listener) {
        this.currencySelectedListener = listener;
    }


}
