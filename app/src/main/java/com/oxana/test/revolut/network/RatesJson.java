package com.oxana.test.revolut.network;


import java.util.HashMap;
import java.util.Map;

import com.google.gson.annotations.Expose;
/**
 * Created by Oksana_Skorniakova on 2/28/17.
 */
public class RatesJson {
    @Expose
    public String base;

    @Expose
    public Map<String, Double> rates = new HashMap<>();
}
