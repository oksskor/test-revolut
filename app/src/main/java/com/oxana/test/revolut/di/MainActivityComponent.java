package com.oxana.test.revolut.di;

import com.oxana.test.revolut.MainActivity;

import dagger.Component;

/**
 * Created by Oksana_Skorniakova on 3/1/17.
 */

@Component(
        modules = {
                RatesModule.class
        }
)
public interface MainActivityComponent {

    void inject(MainActivity activity);

    final class Injector {
        private Injector() {
        }

        public static void inject(MainActivity activity) {
            DaggerMainActivityComponent.builder()
                    .ratesModule(new RatesModule())
                    .build()
                    .inject(activity);
        }
    }

}