package com.oxana.test.revolut.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.oxana.test.revolut.mvp.model.AbstractRepository;
import com.oxana.test.revolut.mvp.view.IView;

/**
 * Created by Oksana_Skorniakova on 3/2/17.
 */

public abstract class FragmentPresenter<V extends IView, M extends AbstractRepository, P extends BasePresenter<IView, AbstractRepository>> extends Fragment {

    protected M model;
    protected V view;
    protected P presenter;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = createView(view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.presenter = createPresenter(this.view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.shutDown();
        view.shutDown();
        presenter = null;
        view = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.attach();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.detach();
    }

    public abstract V createView(View view);

    public abstract P createPresenter(V view);
}
