package com.oxana.test.revolut;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.oxana.test.revolut.mvp.view.DummyView;
import com.oxana.test.revolut.mvp.FragmentPresenter;
import com.oxana.test.revolut.mvp.Presenter;
import com.oxana.test.revolut.mvp.model.AbstractRepository;
import com.oxana.test.revolut.mvp.model.Repository;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Oksana_Skorniakova on 3/2/17.
 */

public class DummyFragment extends FragmentPresenter<DummyView, Repository, Presenter> {


    @BindView(R.id.sum_base)
    EditText sum1;
    @BindView(R.id.sum_con)
    TextView sum2;
    @BindView(R.id.rate_base)
    TextView rate1;
    @BindView(R.id.rate_con)
    TextView rate2;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dummy, container, false);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public DummyView createView(View view) {
        return new DummyView(sum1, sum2, rate1, rate2);
    }

    @Override
    public Presenter createPresenter(DummyView view) {
        Activity activity = getActivity();
        if (activity != null) {
            AbstractRepository repo = ((IApiProvider) activity).getApi().getRepository();
            return new Presenter(view, repo);
        }

        return null;
    }
}
