package com.oxana.test.revolut.mvp.model;

import android.os.Handler;

import java.util.Observable;

/**
 * Created by Oksana_Skorniakova on 2/28/17.
 */

public abstract class AbstractRepository extends Observable implements IEntity {
    private final Handler handler = new Handler();

    private
    @IEntity.EntityState
    int state = EntityState.NA;

    /**
     * Update is going on main Android thread.
     * It's done to give ability for UI components dispatch events on the thread they are working on.
     */
    protected final void updated(@EntityState int state, final @RatesModel.Currency String currency) {
        this.state = state;
        handler.post(new Runnable() {
            @Override
            public void run() {
                setChanged();
                notifyObservers(currency);
            }
        });
    }

    @Override
    public int getState() {
        return state;
    }

    public abstract void restart();

    public abstract void update(@RatesModel.Currency String baseCurrency);

    public abstract RatesModel getRates(@RatesModel.Currency String baseCurrency, boolean update);

    public boolean isAvailable() {
        return getState() == EntityState.AVAILABLE;
    }
}
