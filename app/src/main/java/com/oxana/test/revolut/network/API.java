package com.oxana.test.revolut.network;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Oksana_Skorniakova on 2/28/17.
 */

public interface API {
    @GET("/latest")
    Observable<RatesJson> getRates(@Query("base") String cur);
}
