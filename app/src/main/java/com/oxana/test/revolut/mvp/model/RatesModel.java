package com.oxana.test.revolut.mvp.model;

import android.support.annotation.StringDef;

import com.oxana.test.revolut.network.RatesJson;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.oxana.test.revolut.mvp.model.RatesModel.Currency.EUR;
import static com.oxana.test.revolut.mvp.model.RatesModel.Currency.GBP;
import static com.oxana.test.revolut.mvp.model.RatesModel.Currency.USD;

/**
 * Created by Oksana_Skorniakova on 2/28/17.
 */

public class RatesModel {
    private
    @RatesModel.Currency
    String baseCurrency;

    private Set<RateModel> rates = new HashSet<>();


    public RatesModel(RatesJson json) {
        this(json.base);
        for (Map.Entry<String, Double> entry : json.rates.entrySet()) {
            rates.add(new RateModel(entry.getKey(), entry.getValue()));
        }
    }

    public RatesModel(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public void setRates(Set<RateModel> rates) {
        this.rates = rates;
    }


    public String getBase() {
        return baseCurrency;
    }

    public Double getRate(@RatesModel.Currency String currency) {
        for (RateModel model : rates) {
            if (model.getCurrency().equals(currency)) {
                return model.getRate();
            }
        }
        return null;
    }

    @StringDef({EUR, USD, GBP})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Currency {
        String EUR = "EUR";
        String USD = "USD";
        String GBP = "GBP";
    }


    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RatesModel) {
            RatesModel model = (RatesModel) obj;
            if (this.baseCurrency.equals(model.baseCurrency)) {
                return true;
            }
        }
        return false;
    }


    @Override
    public int hashCode() {
        return baseCurrency.hashCode();
    }

}
