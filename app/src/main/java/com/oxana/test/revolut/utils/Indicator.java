package com.oxana.test.revolut.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.oxana.test.revolut.R;

/**
 * Created by Oksana_Skorniakova on 3/3/17.
 */

public class Indicator extends View implements ViewPager.OnPageChangeListener, ViewPager.OnAdapterChangeListener {

    private ViewPager pager;
    private float radius = 30;
    private float margin;
    private float gap = 30;
    private float y;
    private Paint paintSelected;
    private Paint paintNotSelected;
    private int n = 0;
    private int currentPosition = 0;
    private int indicatorSelectedColor;
    private int indicatorNotSelectedColor;

    public Indicator(Context context) {
        super(context);
        init(context, null);
    }

    public Indicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public Indicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            try {
                TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.Indicator);
                radius = array.getDimension(R.styleable.Indicator_indicator_radius, getResources().getDimension(R.dimen.indicator_radius));
                gap = array.getDimension(R.styleable.Indicator_indicator_gap, getResources().getDimension(R.dimen.indicator_gap));
                indicatorSelectedColor = array.getColor(R.styleable.Indicator_indicator_color_selected, Color.WHITE);
                indicatorNotSelectedColor = array.getColor(R.styleable.Indicator_indicator_color_not_selected, Color.WHITE);
                array.recycle();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        paintNotSelected = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintNotSelected.setColor(indicatorNotSelectedColor);

        paintSelected = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintSelected.setColor(indicatorSelectedColor);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (pager != null && pager.getAdapter() != null) {
            float width = getMeasuredWidth();
            float height = getMeasuredHeight();
            n = pager.getAdapter().getCount();
            y = height / 2;
            if (width - (height * n) > 0) {
                margin = ((width - (gap * n)) / 2) - radius;
            }
        }
    }

    @Override
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        if (pager != null && n > 1) {
            float x = margin + (gap / 2) + radius;
            for (int i = 0; i < n; i++, x += gap) {
                canvas.drawCircle(x, y, radius, paintNotSelected);
            }
            x = (int) ((margin + (gap / 2)) + (currentPosition * gap) + radius);
            canvas.drawCircle(x, y, radius, paintSelected);
        }
    }

    public void setUpWithViewPager(ViewPager pager) {
        this.pager = pager;
        pager.addOnPageChangeListener(this);
        pager.addOnAdapterChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        currentPosition = position;
        invalidate();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onAdapterChanged(@NonNull ViewPager viewPager, @Nullable PagerAdapter oldAdapter, @Nullable PagerAdapter newAdapter) {
        n = newAdapter.getCount();
        requestLayout();
    }
}
