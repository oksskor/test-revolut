package com.oxana.test.revolut;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oxana.test.revolut.mvp.FragmentPresenter;
import com.oxana.test.revolut.mvp.view.PagerView;
import com.oxana.test.revolut.mvp.Presenter;
import com.oxana.test.revolut.mvp.model.AbstractRepository;
import com.oxana.test.revolut.mvp.model.Repository;
import com.oxana.test.revolut.utils.Indicator;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Oksana_Skorniakova on 3/2/17.
 */

public class PagerFragment extends FragmentPresenter<PagerView, Repository, Presenter> {


    @BindView(R.id.pager_base)
    ViewPager pagerBase;
    @BindView(R.id.pager_convert)
    ViewPager pagerConvert;
    @BindView(R.id.indicator_base)
    Indicator indicatorBase;
    @BindView(R.id.indicator_convert)
    Indicator indicatorConvert;

    public static PagerFragment newInstance(){
        return new PagerFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pager, container, false);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public PagerView createView(View view) {
        return new PagerView(pagerBase, pagerConvert, indicatorBase, indicatorConvert);
    }

    @Override
    public Presenter createPresenter(PagerView view) {
        Activity activity = getActivity();
        if (activity != null) {
            AbstractRepository repo = ((IApiProvider) activity).getApi().getRepository();
            return new Presenter(view, repo);
        }

        return null;
    }
}
