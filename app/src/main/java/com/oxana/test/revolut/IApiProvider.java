package com.oxana.test.revolut;

import com.oxana.test.revolut.di.IRepoProvider;

/**
 * Created by Oksana_Skorniakova on 3/2/17.
 */

public interface IApiProvider {
    IRepoProvider getApi();
}
