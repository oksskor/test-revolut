package com.oxana.test.revolut.mvp;

import com.oxana.test.revolut.mvp.model.AbstractRepository;
import com.oxana.test.revolut.mvp.view.IView;

/**
 * Created by Oksana_Skorniakova on 3/2/17.
 */

public abstract class BasePresenter<View extends IView, Repo extends AbstractRepository> {

    private Repo repo;
    private View view;

    public BasePresenter(View view, Repo repo) {
        this.view = view;
        this.repo = repo;
    }

    protected View getView() {
        return view;
    }

    // TODO: maybe rename, topic to discuss
    protected Repo getRepo() {
        return repo;
    }

    public abstract void attach();

    public abstract void detach();

    public void shutDown() {
        this.view = null;
        this.repo = null;
    }
}
