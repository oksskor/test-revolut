package com.oxana.test.revolut;

import com.oxana.test.revolut.mvp.Presenter;
import com.oxana.test.revolut.mvp.model.AbstractRepository;
import com.oxana.test.revolut.mvp.model.RateModel;
import com.oxana.test.revolut.mvp.model.RatesModel;
import com.oxana.test.revolut.mvp.view.IView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Oxana on 06.03.2017.
 */

public class PresenterTest {
    IView view;
    AbstractRepository repo;
    Presenter presenter;

    @Before
    public void setUp() {

        view = Mockito.mock(IView.class);
        repo = Mockito.mock(AbstractRepository.class);
        presenter = new Presenter(view, repo);

        when(view.getBaseCurrency()).thenReturn(RatesModel.Currency.USD);
        when(view.getConvertCurrency()).thenReturn(RatesModel.Currency.GBP);
        when(view.getEnteredSum()).thenReturn(1000d);

        RatesModel models = new RatesModel(RatesModel.Currency.USD);
        Set<RateModel> set = new HashSet<>();
        set.add(new RateModel(RatesModel.Currency.GBP, 1.4768));
        set.add(new RateModel(RatesModel.Currency.EUR, 0.89));
        models.setRates(set);

        when(repo.getRates(RatesModel.Currency.USD, false)).thenReturn(models);
        presenter.attach();
    }

    @Test
    public void testSetValues() {
        presenter.setValues();
        verify(view).setRates(1.4768);
        verify(view).setSum(1476.8);
    }

}
